package com.example.loopingvideoview;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.os.Bundle;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.loopingvideoview.LoopingVideoView.OnPreparedListener;

public class MainActivity extends Activity {
	LinearLayout layout = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final LoopingVideoView video = new LoopingVideoView(this);
		layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);
		final Button btMode = new Button(this);
		final Button btSpeed = new Button(this);
		btMode.setText("normal");
		btSpeed.setText("1.0");

		btSpeed.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(video.getPlaySpeed() == LoopingVideoView.PLAY_SPEED_DEFAULT){
					video.setPlaySpeed(2.0f);
					btSpeed.setText("2.0f");
				}
				else{
					btSpeed.setText("1.0f");
					video.setPlaySpeed(1.0f);
				}
			}
		});
		btMode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(video.getMode() == LoopingVideoView.PLAY_MODE_NORMAL){
					video.setMode(LoopingVideoView.PLAY_MODE_REVERSE);
					btMode.setText("reverse");
				}else{
					video.setMode(LoopingVideoView.PLAY_MODE_NORMAL);
					btMode.setText("normal");
				}
			}
		});

		layout.addView(btMode);
		layout.addView(btSpeed);
		layout.addView(video);
		setContentView(layout);
		
		try {
			new VideoEditorBuilder(this)
			.images(true)
			.setFps(25)
			.build().edit(Environment.getExternalStorageDirectory().getAbsolutePath()+"/editing.mp4", 
					getDir("jpegs", MODE_PRIVATE).getAbsolutePath()+"/%d.jpeg");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
		try {
			video.setDataSource(Environment.getExternalStorageDirectory().getAbsolutePath()+"/editing.mp4", getDir("jpegs", MODE_PRIVATE).getAbsolutePath());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		video.prepareAsync();
		video.setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				// TODO Auto-generated method stub
				//				video.start();
				//				mp.start();
				video.start();
				video.seekTo(5000);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}

class LoopingVideoView extends RelativeLayout{

	public static final int FPS = 25;
	
	//play mode
	public static final int PLAY_MODE_NORMAL = 0;
	public static final int PLAY_MODE_REVERSE = 1;
	public static final int PLAY_MODE_PINGPONG = 2;

	//play speed
	public static final float PLAY_SPEED_MIN = 0.5f;
	public static final float PLAY_SPEED_MAX = 2.0f;
	public static final float PLAY_SPEED_DEFAULT = 1.0f;

	private MediaPlayer mp = null;
	private SurfaceView videoSurfaceView = null;
	private SurfaceView imageSurfaceView = null;
	private SurfaceHolder videoViewHolder = null;
	private SurfaceHolder imageViewHolder = null;
	private String videoUrl = null;
	private File tmpJpegsDir = null;
	private Thread playThread = null;
	private OnPreparedListener preparedListener = null;

	private boolean isDestroy = false;
	private boolean isPlaying = false;
	private boolean isPrepared = false;
	private int pingpongDir = -1;// pingpongmode일 경우, play방향 : -1 역방향, 1 정방향 
	private int playMode = PLAY_MODE_NORMAL;
	private int dur = 0;
	private int currentTime = 0;
	private float playSpeed = PLAY_SPEED_DEFAULT;
	
	private Matrix imageMatrix = new Matrix();

	private int layoutWidth = -1;
	private int videoWidth = -1;
	private int videoHeight = -1;
	private int layoutHeight = -1;

	public LoopingVideoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
		// TODO Auto-generated constructor stub
	}

	public LoopingVideoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		// TODO Auto-generated constructor stub
	}

	public LoopingVideoView(Context context) {
		super(context);
		init();
		// TODO Auto-generated constructor stub
	}
	private void init(){
		mp = new MediaPlayer();
		videoSurfaceView = new SurfaceView(getContext());
//		videoSurfaceView.setZOrderMediaOverlay(true);
		imageSurfaceView = new SurfaceView(getContext());
		imageSurfaceView.setZOrderMediaOverlay(true);
		videoViewHolder = videoSurfaceView.getHolder();
		imageViewHolder = imageSurfaceView.getHolder();
		imageSurfaceView.setVisibility(View.INVISIBLE);
		videoViewHolder.addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width,
					int height) {
//				adjustSurfaceParams();
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				mp.setDisplay(videoViewHolder);
				if(isPrepared)
					start();
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				mp.setDisplay(null);
				if(mp.isPlaying())
					pause();//mp.pause();
			}
		});
		imageViewHolder.addCallback(new SurfaceHolder.Callback() {

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
//				Toast.makeText(getContext(), "surface destroyed", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void surfaceCreated(SurfaceHolder holder) {
//				Toast.makeText(getContext(), "surface created", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width,
					int height) {
			}
		});
		mp.setOnVideoSizeChangedListener(new OnVideoSizeChangedListener() {
			
			@Override
			public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
				// TODO Auto-generated method stub
				
			}
		});
		mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer mp) {
				mp.setLooping(true);
				dur = mp.getDuration();
				videoHeight = mp.getVideoHeight();
				videoWidth = mp.getVideoWidth();
				adjustSurfaceParams();
				isPrepared = true;
				if(preparedListener != null)
					preparedListener.onPrepared(mp);
			}
		});
		tmpJpegsDir = getContext().getDir("LoopingVideoView", Context.MODE_PRIVATE);
		File[] files = tmpJpegsDir.listFiles();
		for(File file : files)
			file.delete();

		playThread = new PlayThread();
		playThread.start();
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
		RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
		params.addRule(RelativeLayout.CENTER_IN_PARENT);
		params1.addRule(RelativeLayout.CENTER_IN_PARENT);
		this.addView(videoSurfaceView, params);
		this.addView(imageSurfaceView, params1);
	}

	public void setDataSource(String videoPath, String jpegsPath) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException{
		mp.setDataSource(videoPath);
		videoUrl = videoPath;
		tmpJpegsDir = new File(jpegsPath);
	}
	public void prepare() throws IllegalStateException, IOException{
		mp.prepare();
	}
	public void prepareAsync(){
		mp.prepareAsync();
	}

	public void setOnPreparedListener(final OnPreparedListener listener){
		preparedListener = listener;
	}

	public void start(){
		isPlaying = true;
		if(playMode != -1)
			setMode(playMode);
		if(playThread.getState() == Thread.State.WAITING){
			synchronized (playThread) {
				playThread.notify();
			}
		}
	}
	public void pause(){
		isPlaying = false;
	}
	public boolean isPlaying(){
		return isPlaying;
	}
	public void seekTo(int timeInMilles){
		currentTime = timeInMilles;
		mp.seekTo(currentTime);
	}
	public int getCurrentTime(){
		return currentTime;
	}
	public void setMode(int mode){//함수내 명령문들의 순서를 바꿀경우 동작하지 않을수있다.(쓰레딩 동기화를 따로 안하고, 명령순서로 정함)
		if(mode == -1)
			return;
		if(mode == PLAY_MODE_NORMAL && playSpeed == PLAY_SPEED_DEFAULT){
			imageSurfaceView.setVisibility(View.INVISIBLE);
//			videoSurfaceView.setVisibility(View.VISIBLE);
		}else{
			imageSurfaceView.setVisibility(View.VISIBLE);
//			videoSurfaceView.setVisibility(View.INVISIBLE);
		}
		playMode = mode;
		if(mp != null){
			if(mp.isPlaying())
				mp.pause();
			switch(playMode){
			case PLAY_MODE_NORMAL:
			case PLAY_MODE_PINGPONG:
				seekTo(0);
				break;
			case PLAY_MODE_REVERSE:
				currentTime = dur;
				break;
			}
		}
			
	}
	public int getMode(){
		return playMode;
	}
	public void setPlaySpeed(float rate){//함수내 명령문들의 순서를 바꿀경우 동작하지 않을수있다.(쓰레딩 동기화를 따로 안하고, 명령순서로 정함)
		if(playMode == PLAY_MODE_NORMAL && rate == PLAY_SPEED_DEFAULT){
			imageSurfaceView.setVisibility(View.INVISIBLE);
//			videoSurfaceView.setVisibility(View.VISIBLE);
		}else{
			imageSurfaceView.setVisibility(View.VISIBLE);
//			videoSurfaceView.setVisibility(View.INVISIBLE);
		}
		if(mp != null){
			if(mp.isPlaying())
				mp.pause();
			switch(playMode){
			case PLAY_MODE_NORMAL:
			case PLAY_MODE_PINGPONG:
				seekTo(0);
				break;
			case PLAY_MODE_REVERSE:
				currentTime = dur;
				break;
			}
		}
		playSpeed = rate;
	}
	public float getPlaySpeed(){
		return playSpeed;
	}

	public void recycle(){
		File[] files = tmpJpegsDir.listFiles();
		for(File file : files)
			file.delete();
		isDestroy = true;
	}
	
	private boolean isSurfaceSized = false;
	private void adjustSurfaceParams(){
		if(isSurfaceSized)
			return;
		if( layoutHeight != -1 && videoHeight != -1){
			isSurfaceSized = true;
			float scale = Math.min((float)layoutWidth/videoWidth, (float)layoutHeight/videoHeight); 
			ViewGroup.LayoutParams params = videoSurfaceView.getLayoutParams();
			ViewGroup.LayoutParams params1 = imageSurfaceView.getLayoutParams();
			params1.width = params.width = (int) (videoWidth*scale);
			params1.height = params.height = (int) (videoHeight*scale);
			imageMatrix.setScale(scale, scale);
			Log.d("bigs"," adjst surface scale:"+scale+" "+layoutWidth+" "+videoWidth+" "+layoutHeight+" "+videoHeight);
		}
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		// TODO Auto-generated method stub
		super.onSizeChanged(w, h, oldw, oldh);
		layoutHeight = h;
		layoutWidth = w;
		adjustSurfaceParams();
	}
	public interface OnPreparedListener{
		public void onPrepared(MediaPlayer mp);
	}

//	public  class	JpegsMakingThread extends Thread{
//		@Override
//		public void run() {
//			// TODO Auto-generated method stub
//			if(videoUrl == null)
//				throw new IllegalStateException("set source before prepare");
//			try {
//				new VideoEditorBuilder(getContext())
//				.images(true)
//				.setFps(FPS)
//				.build().edit(videoUrl, tmpJpegsDir.getAbsolutePath()+"/%d.jpeg");
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
	private class PlayThread extends Thread{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(!isDestroy){
				if(mp == null){
					try {
						Thread.sleep(1000/FPS);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					continue;
				}
				if(isPlaying){
					if(playMode == PLAY_MODE_NORMAL && playSpeed == PLAY_SPEED_DEFAULT){
						if(!mp.isPlaying()){
							mp.start();
						}
						currentTime = mp.getCurrentPosition();
						try {
							Thread.sleep(1000/FPS);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					else{
						if(mp.isPlaying())
							mp.pause();
						long preTime= System.currentTimeMillis();
						if(imageViewHolder != null){
							Canvas canvas = imageViewHolder.lockCanvas();
							if(canvas != null){
								currentTime = getNextTime();//동기화를 위해 선언문의 순서를  요로코롬
								int nexVideoTime = currentTime;
//								Log.d("VideoPreViewActivity", "nextTime : "+nexVideoTime);
//								Log.d("VideoPreViewActivity", "nextFrame : "+ getImageIndex(nexVideoTime));
//								Log.d("VideoPreViewActivity", "canvas : "+ canvas.getClipBounds().toShortString());
								Bitmap bitmap = BitmapFactory.decodeFile(tmpJpegsDir+"/"+getImageIndex(nexVideoTime)+".jpeg");
//								Log.d("VideoPreViewActivity", "bitmap height : "+bitmap.getHeight()+" width : "+bitmap.getWidth());
//								Log.d("VideoPreViewActivity", "video height : "+mp.getVideoHeight()+" width : "+mp.getVideoWidth());
//								Log.d("VideoPreViewActivity", "surfaceview("+imageSurfaceView.getVisibility()+") : height :"+imageSurfaceView.getHeight()+
//										" width : "+imageSurfaceView.getWidth());
//								canvas.drawARGB(0xaa, 0xff, 0xff, 0);
								canvas.drawBitmap(bitmap, imageMatrix, null);
								imageViewHolder.unlockCanvasAndPost(canvas);
								bitmap.recycle();
							}
						}
						long elapsedTime = System.currentTimeMillis() - preTime;
						long idleTime = 1000/FPS - elapsedTime;
						try {
							if(idleTime > 0)
								Thread.sleep(idleTime);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}else{
					synchronized (playThread) {
						try {
							if(mp.isPlaying())
								mp.pause();
							playThread.wait();//play가 아닐경우 대기
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}	
					}
				}
			}
		}
		public void destroy(){
			isDestroy = true;
		}

		private int getNextTime(){
			int nextTime = currentTime;
			switch(playMode){
			case PLAY_MODE_NORMAL:
				nextTime += 1000/FPS * playSpeed;
				if(nextTime > dur)
					nextTime -= dur;
				break;
			case PLAY_MODE_REVERSE:
				nextTime -= 1000/FPS * playSpeed;
				if(nextTime < 0)
					nextTime += dur;
				break;
			case PLAY_MODE_PINGPONG:
				nextTime += pingpongDir * 1000/FPS * playSpeed;
				if(nextTime <= 0){
					nextTime = -nextTime;
					pingpongDir = 1;
				}
				else if(nextTime >= dur){
					nextTime = dur-(nextTime-dur);
					pingpongDir = -1;
				}
				break;
			}
			return nextTime;
		}
		private int getImageIndex(int time){
			return (int) ((float)time/1000*FPS)+(time != dur?1:0);
		}
	}
}
