package com.example.loopingvideoview;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

/**
 * mp4 video editor
 * container : mp4
 * vcodec : h264
 * acodec : aac
 * 
 * libs/liblame.so, assets/ffmpeg가 있어야 동작함.
 * WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE permssion이 요구됌.
 * 
 * builder를 이용해 만들어 사용하길 권장.
 * 만들고 속성 변경후 edit하면 적용 되긴함
 * 
 * 부분적으로 thread-safe함. --> 기본적으로 thread-safe하긴 한데, editing쭝에 속성이 변경되면 이전에 실행한 edit도 변경된 속성이 적용됨.
 * 
 * 이 클래스는 
 * @author bigs
 *
 *내부적으로 인터널 스토리지를 이용하는데, 만약 인터널 스토리지 공간이 부족하다면?
 */
public class VideoEditor {
	private static Object lock = new Object();
	private File ffmpeg = null;
	private Context context = null;
	private boolean onlyKeyFrame = false;
	private boolean reverse = false;
	private int width = -2;
	private int height = -2;
	private int fps = 25;
	private float startSec = -1;
	private float durSec = -1;
	private float playspeed = 1.0f;
	private boolean isPatialExtracted = false;
	private boolean isSizeSet = false;
	private boolean silent = false;
	private boolean images = false;
	private File workingDirectory = null;
	private File tmpDirectory = null;

	private String[] envArgs = null;

	public VideoEditor(Context context){
		this.context = context;
		ffmpeg = new File(context.getDir("ffmpeg", Context.MODE_PRIVATE).getAbsoluteFile()+"/ffmpeg");
		if(!ffmpeg.exists()){
			synchronized (lock) {//double looped checking for thread-safe
				if(!ffmpeg.exists()){
					ffmpeg = new File(context.getDir("ffmpeg", Context.MODE_PRIVATE).getAbsoluteFile()+"/ffmpeg");
					try {
						BufferedInputStream fis = new BufferedInputStream(context.getAssets().open("ffmpeg"));
						ffmpeg.createNewFile();
						FileOutputStream fos = new FileOutputStream(ffmpeg);
						byte[] buffer = new byte[1024];
						while(fis.read(buffer) != -1){
							fos.write(buffer);
						}
						fis.close();
						fos.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(!ffmpeg.canExecute())
						ffmpeg.setExecutable(true);
				}
			}
		}
		envArgs = new String[1];
		envArgs[0] = "LD_LIBRARY_PATH=" + context.getApplicationInfo().nativeLibraryDir;
		tmpDirectory = new File(context.getDir("tmp", Context.MODE_PRIVATE).getAbsolutePath());
		tmpDirectory.mkdir();
	}

	/**
	 * mp4
	 * @param src
	 * @param dest
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public void edit(String src, String dest) throws IOException, InterruptedException{
		Process process = null;
		String[] commands = makeCommand(src, dest);
		if(workingDirectory != null && !workingDirectory.exists())
			throw new IOException("file not found : workingDirectory");
		if(src.startsWith("/") && !new File((workingDirectory==null?"":workingDirectory.getAbsoluteFile()+"/")+src).exists())
			throw new IOException("file not found : src file : "+src);
		if(!reverse){
			process = Runtime.getRuntime().exec(commands[0], envArgs, workingDirectory);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			String line = null;
			while((line = reader.readLine()) != null ){
				Log.d("VideoEditor", line);
			}
			process.waitFor();
		}else{
			File[] files = tmpDirectory.listFiles();
			for(File file : files)
				file.delete();
			process = Runtime.getRuntime().exec(commands[0], envArgs, workingDirectory);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			String line = null;
			while((line = reader.readLine()) != null ){
				Log.d("VideoEditor", line);
			}
			process.waitFor();
			files = tmpDirectory.listFiles();
			int cntFiles = files.length;
			for(File file : files){
				String rename = new String("tmp"+(cntFiles-Integer.parseInt(file.getName().split("\\.")[0])+1))+".jpeg";
				file.renameTo(new File(tmpDirectory, rename));
			}
			process = Runtime.getRuntime().exec(commands[1], envArgs, workingDirectory);
			reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			line = null;
			while((line = reader.readLine()) != null ){
				Log.d("VideoEditor", line);
			}
			process.waitFor();
			files = tmpDirectory.listFiles();
			for(File file : files)
				file.delete();
		}

	}

	/**
	 * 짝수 x 짝수만 가능,
	 * width, height 둘중에 하나를 -2를 주면 -2가 아닌변에 맞춰 기본 비율로 스케일됌
	 * @param width
	 * @param height
	 */
	public void setSize(int width, int height){
		isSizeSet = true;
		this.width = width;
		this.height = height;
	}
	public void setSizeDefault(){
		isSizeSet = false;
		width = height = -2;
	}
	public void onlyKeyFrame(boolean keyFrame){
		this.onlyKeyFrame = keyFrame;
	}
	public void reverse(boolean reverse){
		this.reverse = reverse;
	}
	public void setWorkingDirectory(String dir){
		workingDirectory = new File(dir);
	}
	public void silent(boolean silent){
		this.silent = silent;
	}
	public void images(boolean isImages){
		images = isImages;
	}
	public void setFps(int fps){
		this.fps = fps;
	}
	/**
	 * 일부분만을 추출할 수 있다.
	 * 단위는 초단위이며, 소숫점을 통해 밀리초까지 설정 가능하다.
	 * @param startSec 추출 시작시간
	 * @param durSec 시작시간으로 부터 몇초까지 자를건지
	 */
	public void setExtractTime(float startSec, float durSec){
		isPatialExtracted = true;
		this.startSec = startSec;
		this.durSec = durSec;
	}
	public void setPlaySpeed(float rate){
		this.playspeed = rate;
	}
	private String[] makeCommand(String src, String dest){
		String[] command = null;
		String defCommand = null;
		defCommand = ffmpeg.getAbsolutePath()+
				" -y"+
				" -i "+src+
				" -strict -2"+
				(isPatialExtracted ? " -ss "+startSec+" -t "+durSec: "")+
				(silent ? " -an" : "")+
				(onlyKeyFrame ? " -g 0" : "")+
				(isSizeSet ? " -vf scale="+width+":"+height : "")+
				" -r "+fps+
				" -vf setpts=1/"+playspeed+"*PTS";
		if(!reverse){
			command = new String[1];
			command[0] = defCommand+
					(images ? " -qscale:v 2" :
						(onlyKeyFrame ? " -vcodec h264 -preset ultrafast -profile:v baseline" : " -vcodec copy")+
					" -acodec aac")+//h264 개년 개나느림, 왜지 빌드를 잘못한건가, 원래 arm최적화가 안되어있는건가
					" "+dest;
		}else{
			//reverse
			command = new String[2];
			command[0] = defCommand+
					" -qscale:v 3"+
					" "+tmpDirectory.getAbsolutePath()+"/%d.jpeg";
			command[1] = ffmpeg.getAbsolutePath()+
					" -y"+
					" -f image2"+
					" -r 25"+
					" -vcodec mjpeg"+
					" -i "+tmpDirectory.getAbsolutePath()+"/tmp%d.jpeg"+
					" -vcodec h264 -preset ultrafast -profile:v baseline"+
					" "+dest;
		}
		return command;
	}


}
